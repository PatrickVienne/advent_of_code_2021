import itertools
import time
from collections import Counter, defaultdict


class Task14:
    def __init__(self):
        """Keep track only of pais, we are not interested in the orders

        if we have a rule NN->C

        and template is NNCB

        our pairs are NN, NB, CB
        for each pair, we can then say according to the rule, what new parts will be spawn:

        NN -> [NC, CN]

        We only have to keep track of these, but also remember what was the first and last letter

        NNCB -> 2N, 1C, 1B

        NN
         NC
          CB
             -> Count of letters: (3N,2C,1B + 1N,1B) / 2

        We have to keep track of the initial first and last letter

        """
        self.polymer_template = ""
        self.pair_insertions = {}
        self.creation_rules = {}
        # keep track of first and last, since all other characters will have overlaps
        self.first = ""
        self.last = ""
        self.add_pair_rule = {}
        self.pair_counts = defaultdict(int)

    def read_file(self, filepath: str) -> None:
        with open(filepath, "r") as f:
            self.polymer_template = next(f).strip(" \n")
            self.first = self.polymer_template[0]
            self.last = self.polymer_template[-1]
            self.pair_counts = self.get_pair_counts(self.polymer_template)
            next(f)
            for line in f:
                cleaned_line = line.strip(" \n")
                self.pair_insertions.update(self.parse_instruction(cleaned_line))
                self.add_pair_rule.update(self.parse_new_instruction(cleaned_line))

    @staticmethod
    def get_pair_counts(polymer_template):
        return Counter(polymer_template[idx:idx + 2] for idx in range(len(polymer_template) - 1))

    @staticmethod
    def parse_instruction(line: str) -> dict:
        k, v = line.split(" -> ")
        return {k: v}

    @staticmethod
    def parse_new_instruction(line: str) -> dict:
        k, v = line.split(" -> ")
        return {k: [k[0]+v, v+k[1]]}

    def step(self, n_steps=1):
        """Trick is to go to the last step for each combination, save the counts and remove it from the initial"""
        start = time.time()
        for curr_step in range(n_steps):
            new_counts = defaultdict(int)
            for k, v in self.pair_counts.items():
                for new_parts in self.add_pair_rule.get(k, [k]):
                    new_counts[new_parts] += v
            self.pair_counts = new_counts
            print("", curr_step, time.time() - start)

        #
        # Slow Algo for debugging
        # For fast execution, we do not need to keep track of the order
        #
        # start = time.time()
        # for curr_step in range(n_steps):
        #     print("", curr_step, time.time()-start)
        #     new_template = ""
        #     for idx in range(len(self.polymer_template) - 1):
        #         comb = self.polymer_template[idx:idx + 2]
        #         new_template += comb[0]
        #         new_template += self.pair_insertions.get(comb, "")
        #     new_template += self.polymer_template[-1]
        #     self.polymer_template = new_template

    def get_occurence_counts(self):
        """Chain all double-letter pairs, and count the occurrence of all letters.

        Apply correction for first and last"""
        counts = defaultdict(int)
        for k, v in self.pair_counts.items():
            for letter in k:
                counts[letter] += v
        counts[self.first] += 1
        counts[self.last] += 1
        return {k: v / 2 for k, v in counts.items()}

    def n_most_common_least_common(self):
        c = self.get_occurence_counts()
        return max(c.values()), min(c.values())

    def result(self):
        c = self.get_occurence_counts()
        return max(c.values()) - min(c.values())
