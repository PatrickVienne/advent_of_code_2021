import unittest
import task14


class TestTask14(unittest.TestCase):
    def setUp(self) -> None:
        self.polymer = task14.Task14()

    def test_read_input(self):
        self.polymer.read_file("task14_input_example.txt")
        self.assertEqual(self.polymer.polymer_template, "NNCB")
        self.assertEqual(self.polymer.first, "N")
        self.assertEqual(self.polymer.last, "B")
        self.assertEqual(self.polymer.pair_counts, {'NN': 1, 'NC': 1, 'CB': 1})
        self.assertEqual(self.polymer.pair_insertions, {'BB': 'N',
                                                        'BC': 'B',
                                                        'BH': 'H',
                                                        'BN': 'B',
                                                        'CB': 'H',
                                                        'CC': 'N',
                                                        'CH': 'B',
                                                        'CN': 'C',
                                                        'HB': 'C',
                                                        'HC': 'B',
                                                        'HH': 'N',
                                                        'HN': 'C',
                                                        'NB': 'B',
                                                        'NC': 'B',
                                                        'NH': 'C',
                                                        'NN': 'C'})
        self.assertEqual(self.polymer.add_pair_rule,
                         {'CH': ['CB', 'BH'], 'HH': ['HN', 'NH'], 'CB': ['CH', 'HB'], 'NH': ['NC', 'CH'],
                          'HB': ['HC', 'CB'],
                          'HC': ['HB', 'BC'], 'HN': ['HC', 'CN'], 'NN': ['NC', 'CN'], 'BH': ['BH', 'HH'],
                          'NC': ['NB', 'BC'],
                          'NB': ['NB', 'BB'], 'BN': ['BB', 'BN'], 'BB': ['BN', 'NB'], 'BC': ['BB', 'BC'],
                          'CC': ['CN', 'NC'],
                          'CN': ['CC', 'CN']})

    def test_step_simple(self):
        self.polymer.read_file("task14_input_example.txt")
        # Starting with "NNCB"
        self.assertEqual(self.polymer.pair_counts, {'NN': 1,
                                                    'NC': 1,
                                                    'CB': 1})

        # STEP 1) Transform into "NCNBCHB"
        self.polymer.step()
        self.assertEqual(self.polymer.pair_counts, {'BC': 1,
                                                    'CH': 1,
                                                    'CN': 1,
                                                    'HB': 1,
                                                    'NB': 1,
                                                    'NC': 1})
        # STEP 2) Transform into "NBCCNBBBCBHCB"
        self.polymer.step()
        self.assertEqual(self.polymer.pair_counts, {'BB': 2,
                                                    'BC': 2,
                                                    'BH': 1,
                                                    'CB': 2,
                                                    'CC': 1,
                                                    'CN': 1,
                                                    'HC': 1,
                                                    'NB': 2})
        self.assertEqual(self.polymer.n_most_common_least_common(), (6, 1))

        # STEP 3) Transform into "NBBBCNCCNBBNBNBBCHBHHBCHB"
        self.polymer.step()
        self.assertEqual(self.polymer.pair_counts, {'BB': 4,
                                                    'BC': 3,
                                                    'BH': 1,
                                                    'BN': 2,
                                                    'CC': 1,
                                                    'CH': 2,
                                                    'CN': 2,
                                                    'HB': 3,
                                                    'HH': 1,
                                                    'NB': 4,
                                                    'NC': 1})
        self.assertEqual(self.polymer.n_most_common_least_common(), (11.0, 4.0))

    def test_n_most_common_least_common(self):
        self.polymer.read_file("task14_input_example.txt")
        self.assertEqual(self.polymer.n_most_common_least_common(), (2, 1))

    def test_full_example_10_steps(self):
        self.polymer.read_file("task14_input_example.txt")
        self.polymer.step(10)
        self.assertEqual(self.polymer.n_most_common_least_common(), (1749, 161))
        self.assertEqual(self.polymer.result(), 1588)

    def test_full_example_40_steps(self):
        self.polymer.read_file("task14_input_example.txt")
        self.polymer.step(40)
        self.assertEqual(self.polymer.n_most_common_least_common(), (2192039569602, 3849876073))
        self.assertEqual(self.polymer.result(), 2188189693529)

    def test_full_10_steps(self):
        self.polymer.read_file("task14_input.txt")
        self.polymer.step(10)
        self.assertEqual(self.polymer.n_most_common_least_common(), (3098, 514))
        self.assertEqual(self.polymer.result(), 2584)

    def test_full_40_steps(self):
        self.polymer.read_file("task14_input.txt")
        self.polymer.step(40)
        self.assertEqual(self.polymer.n_most_common_least_common(), (4290780551024, 474383415564))
        self.assertEqual(self.polymer.result(), 3816397135460)


if __name__ == '__main__':
    unittest.main()
