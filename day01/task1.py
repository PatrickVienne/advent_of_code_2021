print("Hello World")

last = None
counter = 0
with open("input.txt") as f:
    last = int(next(f))
    for line in f:
        current = int(line)
        if current > last:
            counter += 1
        print(f"last: {last}, current: {current}, counter: {counter}")
        last = current

print(f'Final Count: {counter}')
