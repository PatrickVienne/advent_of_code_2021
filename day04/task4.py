import re

REG = re.compile("\d+")


def main():
    with open("input.txt") as f:
        for line in f:
            found = REG.findall(line)
            found_numbers = [int(num) for num in found]
            found_numbers = map(int, found)
            print(list(found_numbers))


if __name__ == '__main__':
    main()
