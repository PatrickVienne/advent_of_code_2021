from collections import defaultdict


def line_to_coords(line: str) -> ((int, int), (int, int)):
    coord_1, coord_2 = line.split(' -> ')
    coord_1 = tuple(map(int, coord_1.split(',')))
    coord_2 = tuple(map(int, coord_2.split(',')))
    return coord_1, coord_2


def fill_horizontally(coord_1: (int, int), coord_2: (int, int), n_steam_at_coordinate: dict):
    """Add steam counter to all places reached by the vents horizontally"""
    if coord_1[0] > coord_2[0]:
        coord_2, coord_1 = coord_1, coord_2

    for x in range(coord_1[0], coord_2[0] + 1):
        n_steam_at_coordinate[(x, coord_1[1])] += 1


def fill_vertically(coord_1: (int, int), coord_2: (int, int), n_steam_at_coordinate: dict):
    """Add steam counter to all places reached by the vents vertically"""
    if coord_1[1] > coord_2[1]:
        coord_2, coord_1 = coord_1, coord_2

    for y in range(coord_1[1], coord_2[1] + 1):
        n_steam_at_coordinate[(coord_1[0], y)] += 1


def main(input_file):
    n_steam_at_coordinate = defaultdict(int)
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip(' \n')
            co1, co2 = line_to_coords(line)
            if co1[0] == co2[0]:
                fill_vertically(co1, co2, n_steam_at_coordinate)
            elif co1[1] == co2[1]:
                fill_horizontally(co1, co2, n_steam_at_coordinate)

    n_overlaps = 0
    for x in n_steam_at_coordinate.values():
        if x > 1:
            n_overlaps += 1

    print(f"Found {n_overlaps} overlaps in {input_file}")


if __name__ == '__main__':
    # main('input_task05_test.txt')  # 5
    # main('input_task05_nael.txt') # 7436
    main('input_task05_patrick.txt')  # 5774
